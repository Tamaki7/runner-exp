using UnityEngine;
using System.Collections;

public class Instanciador : MonoBehaviour
{

    public GameObject enemigoPrefab;
    public GameObject timePrefab;
    public float tiempoEntreInstancias = 2f;

    private float tiempoUltimaInstancia;

    void Update()
    {
        if (Time.time > tiempoUltimaInstancia + tiempoEntreInstancias)
        {
            tiempoUltimaInstancia = Time.time;

            Vector3 posicion = new Vector3(Random.Range(-10f, 10f), 0, Random.Range(-10f, 10f));

            Instantiate(enemigoPrefab, posicion, Quaternion.identity);
            Instantiate(timePrefab, posicion, Quaternion.identity);
        }
    }
}
