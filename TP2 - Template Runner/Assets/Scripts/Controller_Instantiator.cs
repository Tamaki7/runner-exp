﻿using System.Collections.Generic;
using UnityEngine;

public class Controller_Instantiator : MonoBehaviour
{
    public List<GameObject> enemies;
    public List<GameObject> Bonus;
    public GameObject timeBonusPrefab;
    public GameObject instantiatePos;
    public float respawningTimer;
    private float time = 0;
    public float timeBonusRespawnTimer;


    void Start()
    {
        Controller_Enemy.enemyVelocity = 2;
    }

    void Update()
    {
        SpawnEnemies();
        SpawnTimeBonus();
        ChangeVelocity();
    }

    private void ChangeVelocity()
    {
        time += Time.deltaTime;
        Controller_Enemy.enemyVelocity = Mathf.SmoothStep(1f, 15f, time / 45f);
    }

    private void SpawnEnemies()
    {
        respawningTimer -= Time.deltaTime;

        if (respawningTimer <= 0)
        {
            Instantiate(enemies[Random.Range(0, enemies.Count)], instantiatePos.transform);
            respawningTimer = Random.Range(2, 6);
        }
    }

    private void SpawnTimeBonus()
    {
        timeBonusRespawnTimer -= Time.deltaTime;

        if (timeBonusRespawnTimer <= 0)
        {
            Instantiate(timeBonusPrefab, instantiatePos.transform);
            timeBonusRespawnTimer = Random.Range(2, 6);
        }
    }
    
}
