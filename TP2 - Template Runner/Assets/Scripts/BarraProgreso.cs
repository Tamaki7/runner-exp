using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BarraProgreso : MonoBehaviour
{
    public Slider BarraPerder;
    public float tiempoMax = 10f;
    private float tiempoActual;

    private void Start()
    {
        tiempoActual = tiempoMax;
    }
    private void Update()
    {
        tiempoActual -= Time.deltaTime;
        if(tiempoActual <= 0f)
        {

            Controller_Hud.gameOver = true;

        }
        BarraPerder.value = tiempoActual / tiempoMax;
    }

    public void AddTime(float amount)
    {
        tiempoActual += amount;
        if(tiempoActual > tiempoMax)
        {
            tiempoActual = tiempoMax;
        }
    }
}