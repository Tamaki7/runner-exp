﻿using UnityEngine;

public class Parallax : MonoBehaviour
{
    public GameObject cam;
    public GameObject jugador;
    private float length, startPos;
    public float parallaxEffect;

    void Start()
    {
        startPos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    void Update()
    {
        transform.position = new Vector3(transform.position.x - parallaxEffect, transform.position.y, transform.position.z);
        if (transform.localPosition.x < -20)
        {
            transform.localPosition = new Vector3(20, transform.localPosition.y, transform.localPosition.z);
        }

        // Detecta si el jugador fue destruido y detiene el movimiento del fondo
        if (jugador == null)
        {
            detenerParallax();
        }
    }

    public void detenerParallax()
    {
        parallaxEffect = 0f;
    }
}